#! /usr/bin/env python

import argparse
import datetime
import glob
import os
import re
import time

from prometheus_client import CollectorRegistry, Gauge, write_to_textfile


class CrashPlan:
    """
        Class to identify crashplan log location
    """

    def __convert_xmx_to_bytes(self, heap_digit, heap_unit):
        """
            Convert Xmx argument into byte integer.
        """

        if not heap_digit.isdigit():
            return 0

        heap_digit = int(heap_digit)

        if heap_unit.lower() == 'k':
            return heap_digit * 1024
        elif heap_unit.lower() == 'm':
            return heap_digit * 1048576
        elif heap_unit.lower() == 'g':
            return heap_digit * 1073741824
        elif heap_unit.lower() == '':
            return heap_digit
        else:
            return 0


    def max_heap_configured(self):
        """
            Extract max heap from run.conf
            Converts to bytes and returns.
        """

        with open(os.path.join(self.installdir, "bin", "run.conf"), "r") as runconf:
            max_heap_search = re.search(r"Xmx(\d+)(\w*)", runconf.read(), flags=re.IGNORECASE)
            if max_heap_search:
                heap_digit, heap_unit = max_heap_search.groups()
                heap_bytes = self.__convert_xmx_to_bytes(heap_digit, heap_unit)
                return heap_bytes
            else:
                return 0



    def __evaluate_oom_timestamp(self, oom_match):
        """
            Evaluates tuple returned by re.findall
            Parses the timestamp and determines whether it's in the time period or not.
        """
        oom_timestamp = datetime.datetime.strptime(oom_match[0], "%m.%d.%y %H:%M:%S.%f")
        if oom_timestamp >= (self.init_time - datetime.timedelta(minutes=self.lookbackminutes)):
            return True
        else:
            return False


    def __search_for_oom(self, service_log):
        """
            Opens each service log and looks for the OOM pattern.
            Passes each to self.__evaluate_oom_timestamp for evaluation and tally.
        """

        log_found_ooms = 0

        with open(service_log, "r") as service_log_fd:
            ooms = re.findall(self.oom_pattern, service_log_fd.read())
            for oom_match in ooms:
                if self.__evaluate_oom_timestamp(oom_match):
                    log_found_ooms = log_found_ooms + 1

        return log_found_ooms
                


    def find_oom(self):
        """
            Identifies service logs that have been modified in the last hour.
            Passes those logs to self.__search_for_oom for analysis.
        """

        # reset to 0 to get an accurate count.
        found_ooms = 0

        # search for all service logs
        service_logs = glob.glob(
            os.path.join(
                self.installdir,
                "log",
                "service.log*",
            )
        )

        # evaluate only the ones modified in our target timeframe.
        for service_log in service_logs:
            if (os.path.getmtime(service_log) + (60*self.lookbackminutes)) >= time.time():
                found_ooms = found_ooms + self.__search_for_oom(service_log)

        return found_ooms


    def __init__(self, installdir="/usr/local/crashplan", lookbackminutes=60):
        
        if not os.path.isdir(installdir):
            raise NotADirectoryError("Crashplan path %s not found." % installdir)

        self.installdir = installdir
        self.init_time = datetime.datetime.now()
        self.lookbackminutes = lookbackminutes
        self.oom_pattern = re.compile(
            r'\[(\d{2}\.\d{2}\.\d{2}\s\d{2}:\d{2}:\d{2}\.\d+).+(OutOfMemoryError)'
        )


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        dest="outputfile",
        default="/usr/local/node_exporter/collections/crashplan.prom"
    )
    parser.add_argument(
        "-c",
        "--crashplan-dir",
        type=str,
        dest="crashplandir",
        default="/usr/local/crashplan",
    )
    parser.add_argument(
        "-l",
        "--lookback-minutes",
        type=int,
        dest="lookback",
        default=60,
    )
    args = parser.parse_args()

    crashplan = CrashPlan(
        installdir=args.crashplandir,
        lookbackminutes=args.lookback,
    )
    
    registry = CollectorRegistry()

    g = Gauge('crashplan_oom_count', 'Count of  OutOfMemoryError occurrances in the Crashplan Service Logs', registry=registry)
    g.set(crashplan.find_oom())

    g = Gauge('crashplan_service_max_heap_bytes', 'Max Heap configured for Crashplan Service', registry=registry)
    g.set(crashplan.max_heap_configured())

    write_to_textfile(args.outputfile, registry)
